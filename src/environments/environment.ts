// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD600IxFAtZwNPIToa49dAX7H1ApS3N2lI',
    authDomain: 'muni-5b22c.firebaseapp.com',
    databaseURL: 'https://muni-5b22c.firebaseio.com',
    projectId: 'muni-5b22c',
    storageBucket: 'muni-5b22c.appspot.com',
    messagingSenderId: '652984838926',
  },
  gmaps: {
    apiKey: 'AIzaSyDfBf9x7ck0Kmjy4mhQQI5xlkkjYD6RECA',
  },
};

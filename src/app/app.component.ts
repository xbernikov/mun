/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {Component, OnInit} from '@angular/core';
import {AnalyticsService, MessagingService} from './@core/utils';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
              private messagingService: MessagingService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
    this.messagingService.getPermission();
    this.messagingService.receiveMessage();

    /*
    this.angularFire.database.ref('users')
    .on('child_added',
      (snapshot: DataSnapshot, prevChildKey) => console.info('key', snapshot.key, 'val', snapshot.val()))
       */
  }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {NoticesComponent} from './notices.component';
import {NoticeListComponent} from './notice-list/notice-list.component';
import {NoticeFormComponent} from './notice-form/notice-form.component';


const routes: Routes = [{
  path: '',
  component: NoticesComponent,
  children: [
    {
      path: 'list',
      component: NoticeListComponent,
    },
    {
      path: 'add',
      component: NoticeFormComponent,
    },
    {
      path: 'edit/:key',
      component: NoticeFormComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoticesRoutingModule {
}

export const routedComponents = [
  NoticesComponent,
  NoticeListComponent,
  NoticeFormComponent,
];

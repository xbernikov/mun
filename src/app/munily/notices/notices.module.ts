import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';

import {ThemeModule} from '../../@theme/theme.module';
import {NoticesRoutingModule, routedComponents} from './notices-routing.module';
import {NoticeService} from '../../@core/services';
import {ComponentsModule} from '../components/components.module';


@NgModule({
  imports: [
    ReactiveFormsModule,
    ThemeModule,
    NoticesRoutingModule,
    Ng2SmartTableModule,
    SweetAlert2Module,
    ComponentsModule,
  ],
  declarations: [...routedComponents],
  providers: [NoticeService],
})
export class NoticesModule {
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {LocalDataSource} from 'ng2-smart-table';
import {Row} from 'ng2-smart-table/lib/data-set/row';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {ToastrService} from 'ngx-toastr';


import {NoticeService} from '@munily/core/services';
import {Notice} from '@munily/core/models';
import {StringService} from '@munily/core/utils';

@Component({
  selector: 'ngx-munily-notice-list',
  templateUrl: './notice-list.component.html',
  styleUrls: ['./notice-list.component.scss'],
})
export class NoticeListComponent implements OnInit {

  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

  selectedNotice: Notice;
  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Acciones',
    },
    pager: {
      perPage: 20,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      title: {
        title: 'Título',
        type: 'string',
      },
      message: {
        title: 'Mensaje',
        type: 'string',
        valuePrepareFunction: (value) => StringService.truncate(value.toString()),
        filter: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private noticeService: NoticeService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.noticeService.getAll()
      .subscribe(
        (notices) => this.source.load(notices),
        (err) => console.error(err),
      )
  }

  onCreate() {
    this.router.navigate(['/munily/notices/add']);
  }

  onDelete({data}: Row) {
    this.selectedNotice = data;
    this.deleteSwal.show();
  }

  onSave({data}: Row) {
    this.router.navigate(['/munily/notices/edit', data.$key]);
  }

  removeNotice() {
    this.toastrService.success('', 'Borrado');
  }

}


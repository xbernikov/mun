import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

import {Notice} from '@munily/core/models';
import {FileService} from '@munily/core/utils';
import {NoticeService} from '@munily/core/services';

// https://medium.com/@justintulk/how-to-query-arrays-of-data-in-firebase-aa28a90181ba

@Component({
  selector: 'ngx-munily-notice-form',
  templateUrl: './notice-form.component.html',
  styleUrls: ['./notice-form.component.scss'],
})
export class NoticeFormComponent implements OnInit {

  private noticeKey: string;
  public currentNotice: Notice;
  public image: string | MSBaseReader;
  public noticeForm: FormGroup;

  public loaded: Boolean = false;
  public submitted: Boolean = false;
  public isNew: Boolean = false;

  constructor(private noticeService: NoticeService,
              private fileService: FileService,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.noticeKey = this.activatedRoute.snapshot.params['key'];
    this.isNew = !this.noticeKey;

    this.noticeService.get(this.noticeKey)
      .subscribe(
        (notice) => {
          console.info('this.notice', notice);
          this.loaded = true;
          this.currentNotice = notice;
          this.image = this.currentNotice.picture ? this.currentNotice.picture.url : null;
          this.setupForm(this.currentNotice);
        },
        err => console.error(err))
  }

  private setupForm(notice: Notice) {
    this.noticeForm = this.formBuilder.group({
      $key: [notice.$key],
      files: [null],
      title: [notice.title, [Validators.required, Validators.minLength(5)]],
      message: [notice.message, [Validators.required, Validators.minLength(20)]],
      status: [notice.status, [Validators.required]],
    });
  }

  onFileSelect(files: FileList) {
    this.fileService.getImage(files[0])
      .then(((blob) => this.image = blob))
      .catch(err => console.error(err));
  }

  onSubmit(event: Event, form: FormGroup): void {
    this.submitted = true;
    this.currentNotice = Notice.fromForm(<Notice>form.value);
    this.noticeService.push(this.currentNotice, !this.isNew)
      .subscribe(
        () => {
          this.submitted = false;
          this.router.navigate(['/munily/notices/list']);
          this.toastrService.success('', `${this.isNew ? 'Guardado' : 'Actualizado'}`);
        },
        err => console.error(err),
      );
  }

  backToList() {
    this.router.navigate(['/munily/notices/list']);
  }

}

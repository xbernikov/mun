import {Component} from '@angular/core';

@Component({
  selector: 'ngx-munily-notices',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class NoticesComponent {
}

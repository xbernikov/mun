import { NgModule } from '@angular/core';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
// import { ImageSelectorComponent } from '../components/image-selector/image-selector.component';
import { ComponentsModule } from '../components/components.module';

import { AreasRoutingModule, routedComponents } from './areas-routing.module';
import { AreaService } from '../../@core/services/area.service';

@NgModule({
  imports: [
    ThemeModule,
    AreasRoutingModule,
    Ng2SmartTableModule,
    SweetAlert2Module,
    ReactiveFormsModule,
    ComponentsModule,
  ],
  declarations: [...routedComponents],
  providers: [AreaService],
})
export class AreasModule {
}

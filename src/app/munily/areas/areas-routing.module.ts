import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AreasComponent } from './areas.component';
import { AreaListComponent } from './area-list/area-list.component';
import { AreaFormComponent } from './area-form/area-form.component';


const routes: Routes = [{
  path: '',
  component: AreasComponent,
  children: [
    {
      path: 'list',
      component: AreaListComponent,
    },
    {
      path: 'add',
      component: AreaFormComponent,
    },
    {
      path: 'edit/:key',
      component: AreaFormComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AreasRoutingModule { }


export const routedComponents = [
  AreasComponent,
  AreaListComponent,
  AreaFormComponent,
];

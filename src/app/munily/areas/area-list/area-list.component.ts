import { Component, OnInit, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Row } from 'ng2-smart-table/lib/data-set/row';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

import { AreaService } from '../../../@core/services/area.service';
import { Area } from '../../../@core/models/area';

@Component({
  selector: 'ngx-munily-area-list',
  templateUrl: './area-list.component.html',
  styleUrls: ['./area-list.component.scss'],
})
export class AreaListComponent implements OnInit {

  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

  selectedArea: Area;

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Acciones',
    },
    pager: {
      perPage: 10,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Nombre',
        type: 'String',
      },
      schedule: {
        title: 'Horario',
        type: 'Array<Schedule>',
        sort: false,
        filter: false,
      },
      isBookable: {
        title: 'Es reservable?',
        type: 'boolean',
        filter: false,
        sort: true,
      },
      status: {
        title: 'Status',
        type: 'string',
        filter: false,
        sort: true,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private areaService: AreaService, private router: Router, private toastrService: ToastrService) {
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  ngOnInit() {
    this.areaService.getAll()
      .subscribe(
        (areas) => {
          console.info('area', areas);
          this.source.load(areas)
        },
        (err) => console.error(err),
    )
  }

  onCreate() {
    this.router.navigate(['/munily/areas/add']);
  }

  onDelete({ data }: Row) {
    this.selectedArea = data;
    this.deleteSwal.show();
  }

  onSave({ data }: Row) {
    this.router.navigate(['/munily/areas/edit', data.$key]);
  }

  removeArea() {
    this.areaService.remove(this.selectedArea);
    this.toastrService.success('', 'Borrado');
  }

}







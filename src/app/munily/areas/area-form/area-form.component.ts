import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; // FormControl
// import { FileService } from '../../../@core/utils';
import {ToastrService} from 'ngx-toastr';

import { Area } from '../../../@core/models';
import { AreaService } from '../../../@core/services/area.service';

@Component({
  selector: 'ngx-munily-area-form',
  templateUrl: './area-form.component.html',
  styleUrls: ['./area-form.component.scss'],
})

export class AreaFormComponent implements OnInit {

  private areaKey: string;
  public currentArea: Area;
  public image: string | MSBaseReader;
  public areaForm: FormGroup;

  public isLoading: Boolean = false;
  public submitted: Boolean = false;
  public isNew: Boolean = false;

  constructor(private areaService: AreaService,
    // private fileService: FileService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService) { }

  /*get areaImage(): FormControl {
    return <FormControl>this.areaForm.get('image');
  }*/

  ngOnInit() {
    this.areaKey = this.activatedRoute.snapshot.params['key'];
    this.isNew = !this.areaKey;

    this.areaService.get(this.areaKey)
      .subscribe(
        (area) => {
          this.isLoading = true;
          this.currentArea = area;
          // this.image = this.currentArea.picture ? this.currentArea.picture.url : null;
          this.setupForm(this.currentArea);
        },
        err => console.error(err))
  }

  private setupForm(area: Area) {
    this.areaForm = this.formBuilder.group({
      $key: [area.$key],
      // files: [null],
      name: [area.name, [Validators.required, Validators.minLength(5)]],
      isBookable: [area.isBookable, [Validators.required]],
      status: [area.status, [Validators.required]],
      // schedule: [area.schedule, [Validators.required]],
    });
  }

  /*onFileSelect(files: FileList) {
    this.fileService.getImage(files[0])
      .then(((blob) => this.image = blob))
      .catch(err => console.error(err));
  }*/

  onSubmit(event: Event, form: FormGroup): void {
    this.submitted = true;
    this.currentArea = Area.fromForm(<Area>form.value);
    this.areaService.save(this.currentArea, !this.isNew)
      .subscribe(
        () => {
          this.submitted = false;
          this.router.navigate(['/munily/areas/list']);
          this.toastrService.success('', `${this.isNew ? 'Guardado' : 'Actualizado'}`);
        },
        err => console.error(err),
      );
  }

}

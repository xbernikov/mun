import {Component} from '@angular/core';

@Component({
  selector: 'ngx-munily-areas',
  template: `<router-outlet></router-outlet>`,
})
export class AreasComponent {}

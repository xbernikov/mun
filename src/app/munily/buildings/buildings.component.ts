import { Component } from '@angular/core';

@Component({
  selector: 'ngx-munily-buildings',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class BuildingsComponent { }

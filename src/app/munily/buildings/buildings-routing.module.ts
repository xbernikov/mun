import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildingsComponent } from './buildings.component';
import { BuildingsListComponent } from './buildings-list/buildings-list.component';
import { BuildingFormComponent } from './building-form/building-form.component';

const routes: Routes = [
  {
    path: '',
    component: BuildingsComponent,
    children: [{
      path: 'list',
      component: BuildingsListComponent,
    }, {
      path: 'new',
      component: BuildingFormComponent,
    }, {
      path: ':key',
      component: BuildingFormComponent,
    }, {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuildingsRoutingModule { }

export const routedComponents = [
  BuildingsComponent,
  BuildingsListComponent,
  BuildingFormComponent,
];

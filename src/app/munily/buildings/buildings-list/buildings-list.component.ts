import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LocalDataSource } from 'ng2-smart-table';
import { Row } from 'ng2-smart-table/lib/data-set/row';

import { BuildingService } from '../../../@core/services';
import { StringService } from '../../../@core/utils/string.service';

@Component({
  selector: 'ngx-munily-buildings-list',
  templateUrl: './buildings-list.component.html',
  styleUrls: ['./buildings-list.component.scss'],
})
export class BuildingsListComponent {

  listTableSettings = {
    mode: 'external',
    actions: {
      position: 'right',
      columnTitle: 'Acciones',
    },
    pager: {
      perPage: 20,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: false,
    },
    columns: {
      name: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: name => StringService.truncate(name.toString()),
      },
      details: {
        title: 'Detalles',
        type: 'string',
        valuePrepareFunction: details => StringService.truncate(details.toString()),
      },
      address: {
        title: 'Dirección',
        type: 'string',
        valuePrepareFunction: address => StringService.truncate(address.toString()),
      },
      isActive: {
        title: 'Estado',
        type: 'string',
        valuePrepareFunction: isActive => isActive ? 'Activo' : 'Inactivo',
      },
    },
  };

  private buildingsSource: LocalDataSource = new LocalDataSource([]);

  constructor(
    private buildingService: BuildingService,
    private router: Router) {

    // Load buildings
    this.buildingService.getBuildings().subscribe( buildings => {
      this.buildingsSource.load(buildings.map( building => {
        building['address'] = building.location.address;
        return building;
      }));
    })
  }

  /**
   * Called when new building is pressed
   */
  createPressed() {
    this.router.navigate(['/munily/buildings/new']);
  }

  onEdit({ data }: Row) {
    this.router.navigate(['/munily/buildings', data.key]);
  }
}

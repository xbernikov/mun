import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Coordinates, Building } from '../../../@core/models';
import { BuildingService } from '../../../@core/services';
import { FileService } from '../../../@core/utils';

@Component({
  selector: 'ngx-munily-building-form',
  templateUrl: './building-form.component.html',
  styleUrls: ['./building-form.component.scss'],
})
export class BuildingFormComponent implements OnInit {

  private buildingKey: string;
  public buildingForm: FormGroup;
  public building: Building;

  public buildingImage: string | MSBaseReader;

  isMap = false;
  triedToSubmit = false;

  name: string;
  details: string;
  address: string;
  coordinates: Coordinates;
  activeBuilding: boolean = true;

  constructor(
    private buildingService: BuildingService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private fileService: FileService) { }

  ngOnInit() {
    this.getCurrentLocation();
    this.buildingKey = this.activatedRoute.snapshot.params['key'];

    if (this.buildingKey) {
      this.buildingService.getBuilding(this.buildingKey).subscribe( building => {
        // console.log(`Building obtained ${JSON.stringify(building)}`);
        this.building = building;
        this.setForm(building);
      });
    }
  }

  private setForm(building: Building) {
    this.buildingForm = this.formBuilder.group({
      key: [building.key],
      files: [null],
      name: [building.name, [Validators.required, Validators.minLength(5)]],
      details: [building.details, [Validators.required, Validators.minLength(5)]],
      isActive: [building.isActive],
      address: [building.location.address],
      coordinate: [building.location.point],
      admins: [building.admins],
    });

    this.listenValuesChanges();
  }

  listenValuesChanges(): void {
    // console.log(`Setting listeners`);
    this.buildingForm.get('address').valueChanges.subscribe( address => {
      this.isMap = false;
      // console.log(`Address changed dude ${address}`);
    });
    setTimeout(() => {
      // console.log(`Executing timeout`);
      this.building.location.address = 'Que xopa chamaco';
    }, 2000);
    /*this.buildingForm.get('coordinate').valueChanges( (coordinate: Coordinates) => {
      console.log(`Coordinate changed dude ${coordinate}`);
    });*/
  }

  /**
   * Try to get user location and show on map
   */
  private getCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition( position => {
        this.coordinates = position.coords;
        this.isMap = true;
      })
    }
  }

  /**
   * Callend when autocomplete update a location
   * @param event
   */
  positionChanged(event: Coordinates) {
    this.coordinates = event;
  }

  /**
   * Called when autocomplete updated an address
   * @param event
   */
  addressChanged(event: string) {
    this.isMap = false;
    this.address = event;
    // console.log(`Address changed called ${event}`);
  }

  /**
   * Callend when map update a location
   * @param event
   */
  mapsCoordinatesChanged(event: Coordinates) {
    this.isMap = true;
    this.coordinates = event;
  }

  onFileSelect(files: FileList) {
    this.fileService.getImage(files[0])
      .then(((blob) => this.buildingImage = blob))
      .catch(err => console.error(err));
  }

  /**
   * Create a building with defined parameters
   */
  createBuilding() {

    // const nBuilding = this.buildingForm.value;
    // console.log(`Form building ${nBuilding}`);


    this.triedToSubmit = true;
    this.buildingService.create(
      this.name,
      this.details,
      this.address,
      this.coordinates,
      this.activeBuilding).subscribe(() => {
      alert('Building created successfully');
    }, error => {
      alert(`Error ocurred ${error}`);
    });
  }
}

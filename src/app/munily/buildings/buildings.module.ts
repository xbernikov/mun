import { NgModule } from '@angular/core';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { SmartTableService } from '../../@core/data/smart-table.service';

import { BuildingsRoutingModule, routedComponents } from './buildings-routing.module';
import { ComponentsModule } from '../components/components.module';

const MODULES = [
  BuildingsRoutingModule,
  ThemeModule,
  Ng2SmartTableModule,
  ComponentsModule,
];

const PROVIDERS = [
  SmartTableService,
];

const COMPONENTS = [];

@NgModule({
  imports: [
    ...MODULES,
  ],
  declarations: [
    ...routedComponents,
    ...COMPONENTS,
  ],
  providers: [
    ...PROVIDERS,
  ],
})
export class BuildingsModule { }

import {Component, OnInit} from '@angular/core';
import {NbMenuItem} from '@nebular/theme';
import {AuthService} from '@munily/core/services';
import {User} from '@munily/core/models';
import {MENU_ITEMS, MenuItem} from './munily-menu';
import {ArrayService} from '@munily/core/utils';

@Component({
  selector: 'ngx-munily',
  template: `
    <ngx-base-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-base-layout>`,
  styleUrls: ['./munily.component.scss'],
})
export class MunilyComponent implements OnInit {

  menu: Array<NbMenuItem> = [];

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.user
      .subscribe(
        (user: User) => this.menu = this.getMenu(user),
        err => console.error(err));
  }

  getMenu(user: User): Array<NbMenuItem> {
    return MENU_ITEMS
      .filter((menu: MenuItem) => ArrayService.containsSome(menu.role, user.$roles))
      .map((menu: MenuItem) => menu.item);
  }


}

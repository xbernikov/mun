import {NbMenuItem} from '@nebular/theme';
import {Role} from '@munily/core/models';

export class MenuItem {
  item: NbMenuItem;
  role: Array<string>;
}

export const MENU_ITEMS: Array<MenuItem> = [
  {
    item: {
      title: 'Dashboard',
      icon: 'nb-home',
      link: '/pages/dashboard',
      home: true,
    },
    role: [Role.su],
  },
  {
    item: {
      title: 'FEATURES',
      group: true,
    },
    role: [Role.su, Role.admin, Role.security, Role.user],
  },
  {
    item: {
      title: 'Edificios',
      icon: 'fa fa-building',
      link: '/munily/buildings',
    },
    role: [Role.su],
  },
  {
    item: {
      title: 'Areas',
      icon: 'fa fa-th-large',
      link: '/munily/areas',
    },
    role: [Role.admin],
  },
  {
    item: {
      title: 'Administradores',
      icon: 'fa fa-user',
      link: '/munily/users/list/admin',
    },
    role: [Role.su],
  },
  {
    item: {
      title: 'Inquilinos',
      icon: 'fa fa-users',
      link: '/munily/users',
    },
    role: [Role.admin],
  },
  {
    item: {
      title: 'Seguridad',
      icon: 'fa fa-user-secret',
      link: '/munily/users/list/security',
    },
    role: [Role.admin],
  },
  {
    item: {
      title: 'Mensajes',
      icon: 'fa fa-comments',
      link: '/munily/messages',
    },
    role: [Role.admin, Role.security],
  },
  {
    item: {
      title: 'Avisos',
      icon: 'fa fa-comment',
      link: '/munily/notices/list',
    },
    role: [Role.admin],
  },
];

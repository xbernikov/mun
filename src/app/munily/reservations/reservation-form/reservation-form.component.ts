import { Component, OnInit } from '@angular/core';
import { CalendarEvent, CalendarEventAction } from 'angular-calendar';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  // isSameDay,
  // isSameMonth,
  // addHours,
} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { AreaService } from '../../../@core/services/area.service';
import { UserService } from '../../../@core/services';

const colors: any = {
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
};

@Component({
  selector: 'ngx-munily-reservation-form',
  templateUrl: './reservation-form.component.html',
})
export class ReservationFormComponent implements OnInit {

  viewDate: Date = new Date();

  // activeDayIsOpen: boolean = true;
  refresh: Subject<any> = new Subject();

  areas = []
  users = []

  constructor(private areaService: AreaService,
              private userService: UserService) {}

  ngOnInit() {
     this.areaService.getAll()
     .subscribe(
     (areas) =>  this.areas = areas.map(area => area.name),
     (err) => console.error(err),
   )
    // console.info('areas', this.areas)
    this.userService.getUsers()
      .subscribe(
        (users) =>  {
          // console.info('users', users)
          this.users = users.map(user =>  user.email)},
      (err) => console.error(err),
    )
   // console.info('users', this.users)
  }


  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];

  events: CalendarEvent[] = [
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: '',
      color: colors.blue,
    },
  ];
  /*dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }*/

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
    });
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    // this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: 'lg' });
  }

}

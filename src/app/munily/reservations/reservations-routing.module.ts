import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReservationsComponent} from './reservations.component';
import {ReservationListComponent} from './reservation-list/reservation-list.component';
import {ReservationCalendarComponent} from './reservation-calendar/reservation-calendar.component';
import {ReservationFormComponent} from './reservation-form/reservation-form.component';

const routes: Routes = [{
  path: '',
  component: ReservationsComponent,
  children: [
    {
      path: 'add',
      component: ReservationFormComponent,
    },
    {
      path: 'calendar',
      component: ReservationCalendarComponent,
    },
    {
      path: 'list',
      component: ReservationListComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationsRoutingModule {}

export const routedComponents = [
  ReservationsComponent,
  ReservationListComponent,
  ReservationCalendarComponent,
  ReservationFormComponent,
];

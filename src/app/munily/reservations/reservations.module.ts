import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';

import {ThemeModule} from '../../@theme/theme.module';
import {ReservationsRoutingModule, routedComponents} from './reservations-routing.module';
import { ReservationService } from '../../@core/services';

import {CalendarDayModule, CalendarModule, CalendarCommonModule, CalendarWeekModule} from 'angular-calendar';
import { DemoUtilsModule } from '../../@theme/components/demo-utils/module';

import { NgSelectModule } from '@ng-select/ng-select';

import { AreaService } from '../../@core/services/area.service';
import { ComponentsModule } from '../components/components.module';



@NgModule({
  imports: [
    ReactiveFormsModule,
    ThemeModule,
    ReservationsRoutingModule,
    Ng2SmartTableModule,
    SweetAlert2Module,
    ComponentsModule,
    // Calendar module
    CalendarModule.forRoot(),
    CalendarDayModule,
    CalendarCommonModule,
    CalendarWeekModule,
    DemoUtilsModule,
    // dropdown
    NgSelectModule,
  ],
  declarations: [...routedComponents],
  providers: [ReservationService, AreaService],
})
export class ReservationModule {
}

import {Component, OnInit, ViewChild} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReservationService } from '../../../@core/services';
import moment = require('moment');
import {Row} from 'ng2-smart-table/lib/data-set/row';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

import { SwalComponent } from '@toverux/ngx-sweetalert2';

const rejectedStatus = 'rejected'
const aprovedStatus = 'approved'

@Component({
  selector: 'ngx-reservation-list',
  templateUrl: './reservation-list.component.html',
})
export class ReservationListComponent implements OnInit {
  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Acciones',
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="ion-ios-checkmark"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-close"></i>',
    },
    columns: {
      apartment: {
        title: 'Apartamento',
        type: 'string',
      },
      area: {
        title: 'Area',
        type: 'string',
      },
      dateOut: {
        title: 'Fecha',
        valuePrepareFunction: (date) =>
          moment(new Date(date * 1000)).format('MMMM, D'),
        type: 'string',
      },
      dateIn: {
        title: 'Fecha',
        valuePrepareFunction: (value, {dateIn, dateOut}) => `${moment(new Date(dateIn * 1000)).format('h:mm a')} -
           ${moment(new Date(dateOut * 1000)).format('h:mm a')} `,
        type: 'string',
      },
      status: {
        title: 'Status',
        type: 'number',
      },
    },
  };



   source: LocalDataSource = new LocalDataSource()

  constructor(private reservationService: ReservationService,
              private router: Router,
              private toastrService: ToastrService) {}

  ngOnInit() {
    this.reservationService.getAreasReserved()
      .subscribe(
        (reservations) => this.source.load(reservations),
        (err) => console.error(err),
      )
  }

  onCreate() {
    // console.info('onCreate');
    this.router.navigate(['/munily/reservations/add']);
  }

  onRejected({data}: Row) {
    // console.info('onRejected', data);
    const reservation = data

    if (reservation.status === aprovedStatus) {
      this.deleteSwal.show();
    } else {
      reservation.status = rejectedStatus
    }

    this.toastrService.success('this is rejected', 'The reservation');
    this.reservationService.update(reservation)
  }

  onAproved({data}: Row) {
    const reservation = data
    reservation.status = aprovedStatus
    this.toastrService.success('this is approved', 'The reservation');
    this.reservationService.update(reservation)
  }
  onCalendar() {
    this.router.navigate(['/munily/reservations/calendar']);
  }

  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      {
        field: 'id',
        search: query,
      },
      {
        field: 'name',
        search: query,
      },
      {
        field: 'username',
        search: query,
      },
      {
        field: 'email',
        search: query,
      },
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-munily-reservations',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ReservationsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

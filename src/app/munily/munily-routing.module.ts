import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MunilyComponent} from './munily.component';

const routes: Routes = [
  {
    path: '', component: MunilyComponent,
    children: [
      {
        path: 'reservations',
        loadChildren: './reservations/reservations.module#ReservationModule',
      },
      {
        path: 'areas',
        loadChildren: './areas/areas.module#AreasModule',
      },
      {
        path: 'buildings',
        loadChildren: './buildings/buildings.module#BuildingsModule',
      }, 
      {
        path: 'messages',
        loadChildren: './messages/messages.module#MessagesModule',
      },
      {
        path: 'notices',
        loadChildren: './notices/notices.module#NoticesModule',
      },
      {
        path: 'users',
        loadChildren: './users/user.module#UsersModule',
      },
      {
        path: '',
        redirectTo: 'areas',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunilyRoutingModule {
}

import {Component} from '@angular/core';

@Component({
  selector: 'ngx-munily-users',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class UsersComponent {
}

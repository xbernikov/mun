import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';

import {ThemeModule} from '../../@theme/theme.module';
import {routedComponents, UsersRoutingModule} from './user-routing.module';
import {NoticeService} from '../../@core/services';
import {ComponentsModule} from '../components/components.module';
import {PasswordService} from '../../@core/utils';


@NgModule({
  imports: [
    ReactiveFormsModule,
    ThemeModule,
    UsersRoutingModule,
    Ng2SmartTableModule,
    SweetAlert2Module,
    ComponentsModule,
  ],
  declarations: [...routedComponents],
  providers: [NoticeService, PasswordService],
})
export class UsersModule {
}

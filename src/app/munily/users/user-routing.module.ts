import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {UsersComponent} from './user.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserFormComponent} from './user-form/user-form.component';


const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: 'list/:type',
      component: UserListComponent,
    },
    {
      path: 'add/:type',
      component: UserFormComponent,
    },
    {
      path: 'edit/:key',
      component: UserFormComponent,
    },
    {
      path: '',
      redirectTo: 'list/user',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {
}

export const routedComponents = [
  UsersComponent,
  UserListComponent,
  UserFormComponent,
];

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {User, UsersPath} from '@munily/core/models';
import {FileService, PasswordService} from '@munily/core/utils';
import {AuthService, UserService} from '@munily/core/services';

@Component({
  selector: 'ngx-munily-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  public userType: UsersPath;
  private userKey: string;
  public meUser: User;
  public currentUser: User;
  public image: string | MSBaseReader;
  public userForm: FormGroup;

  public loaded: Boolean = false;
  public submitted: Boolean = false;
  public isNew: Boolean = false;

  constructor(private authService: AuthService,
              private userService: UserService,
              private fileService: FileService,
              private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.userType = this.activatedRoute.snapshot.params['type'];
    this.userKey = this.activatedRoute.snapshot.params['key'];
    this.isNew = !this.userKey;

    this.authService.user.subscribe((user: User) => {
      this.meUser = user;

      this.userService.get(this.userType, this.userKey)
        .subscribe(
          (editUser) => {
            this.loaded = true;
            this.currentUser = editUser;
            this.image = this.currentUser.picture ? this.currentUser.picture.url : null;
            this.setupForm(this.currentUser);
          },
          err => console.error(err))
    }, err => console.error(err));
  }

  private setupForm(user: User) {
    this.userForm = this.formBuilder.group({
      uid: [user.uid],
      email: [user.email, Validators.required, Validators.minLength(10)],
      displayName: [user.displayName, Validators.required, Validators.minLength(10)],
      password: [PasswordService.generate(10), Validators.required],
      files: [null],
      buildings: [user.buildings],
      apartments: [user.apartments],
    });
  }

  onFileSelect(files: FileList) {
    this.fileService.getImage(files[0])
      .then(((blob) => this.image = blob))
      .catch(err => console.error(err));
  }

  onSubmit(event: Event, form: FormGroup): void {
    this.submitted = true;
    this.currentUser = User.fromForm(<User>form.value);
  }

  backToList() {
    this.router.navigate(['/munily/users/list']);
  }

  onGeneratePassword() {
    (<FormControl>this.userForm.get('password')).setValue(PasswordService.generate(10));
  }

}

import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalDataSource} from 'ng2-smart-table';
import {Row} from 'ng2-smart-table/lib/data-set/row';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {ToastrService} from 'ngx-toastr';

import {User, UsersPath} from '@munily/core/models';
import {StringService} from '@munily/core/utils';
import {UserService} from '@munily/core/services';

// https://plnkr.co/edit/lGP4XsO64gjgnTg9Qpvn?p=preview

@Component({
  selector: 'ngx-munily-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {

  @ViewChild('deleteSwal') private deleteSwal: SwalComponent;
  private userType: UsersPath;
  selectedUser: User;

  settings = {
    mode: 'external',
    actions: {
      columnTitle: 'Acciones',
    },
    pager: {
      perPage: 20,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      title: {
        title: 'Título',
        type: 'string',
      },
      message: {
        title: 'Mensaje',
        type: 'string',
        valuePrepareFunction: (value) => StringService.truncate(value.toString()),
        filter: false,
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private userService: UserService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private toastrService: ToastrService) {
  }

  ngOnInit() {
    this.userType = this.activatedRoute.snapshot.params['type'];
    this.userService.getUsers(this.userType)
      .subscribe(
        (users) => this.source.load(users),
        (err) => console.error(err),
      )
  }

  onCreate() {
    this.router.navigate(['/munily/users/add', this.userType]);
  }

  onDelete({data}: Row) {
    this.selectedUser = data;
    this.deleteSwal.show();
  }

  onSave({data}: Row) {
    this.router.navigate(['/munily/users/edit', data.$key]);
  }

  removeNotice() {
    this.toastrService.success('', 'Borrado');
  }

}


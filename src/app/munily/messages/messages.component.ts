/**
 * Created by jaimevillar on 02/23/18.
 */
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../@core/services/message.service';
import { AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { CurrentChat } from '../../@core/models/chat.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from '../../@core/services/user.service';

@Component({
  selector: 'ngx-munily-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
})

export class MessagesComponent implements OnInit {
  allUsers: any = null;
  contacts: any = null;
  messages: any = null;
  newMessage: any = null;
  selectedContact: boolean = false;
  public submitted: Boolean = false;
  public messageForm: FormGroup;
  public message: AbstractControl;

  currentChat: CurrentChat = {
    exists: false,
    contact: {
      name: '',
      uid: '',
    },
    chat: {
      contactId: '',
      hour_last_message: '',
      last_message: '',
      messages: null,
      messagesUrl: '',
      myUserId: '',
    },
  };

  authUser: any;
  user: any;
  emptyContacts: boolean = false;

  constructor(private messageService: MessageService,
              private angularFireAuth: AngularFireAuth,
              private userService: UserService,
              private formBuilder: FormBuilder) {

    this.messageForm = this.formBuilder.group({
      'message': ['', Validators.compose([Validators.required])],
    });
    this.message = this.messageForm.controls['message'];

    this.getUser();
  }

  ngOnInit() {
  }

  getUser() {
    this.angularFireAuth.authState.subscribe((authUser) => {
      this.authUser = authUser;
      console.info('auth user uid: ', this.authUser.uid);
      this.userService.get('/users', this.authUser.uid).subscribe((user) => {
        this.user = user;
        console.info('user: ', this.user);
        this.getContacts();
      });
    });
  }

  getContacts(): void {
    this.messageService.getContactListByBuilding(this.user.build)
      .subscribe(usersList => {
        this.allUsers = usersList;
        this.setContacts(this.user.uid);
      });
  }

  setContacts(userId: string): void {
    this.contacts = this.allUsers.filter(function( contact ) {
      return contact.uid !== userId;
    });
    if (this.contacts.length === 0) {
      this.emptyContacts = true;
    }
  }

  getMyChatWithContact(contactId: string, contactName: string): void {
    this.selectedContact = true;
    this.currentChat.contact = { name: contactName, uid: contactId };
    this.messageService.getChat(this.user.uid, this.currentChat.contact.uid)
      .subscribe(chat => {
          this.currentChat.chat = chat;
          if (this.currentChat.chat !== null) {
            if (this.currentChat.chat.messagesUrl !== '') {
              console.info('chat exists..');
              this.currentChat.exists = true;
              this.getMessages();
            } else {
              console.info('chat messages dont exists..');
              this.currentChat.exists = false;
            }
          } else {
            this.prepareChat();
            this.currentChat.exists = false;
          }
      });
  }

  getMessages(): void {
    this.messageService.getMessages(this.currentChat.chat.messagesUrl)
      .subscribe(messagesList => {
        this.messages = messagesList;
      });
  }

  sendMessage(message: any): void {
    if (this.currentChat.exists) {
      console.info('update chat');
      this.sendMessageToChat(message);
    } else {
      console.info('create new chat');
      this.createNewChat(message)
    }
    this.messageForm.reset();
    this.submitted = false;
  }

  createNewChat(message: any): void {
    this.messageService.createChat(this.user.uid, this.currentChat.contact.uid, message);
  }

  sendMessageToChat(message: any): void {
    this.messageService.updateChat(
      this.user.uid,
      this.currentChat.contact.uid,
      this.currentChat.chat.messagesUrl,
      message);
  }

  onSubmit(form: FormGroup) {
    this.submitted = true;
    const currentDate = new Date();

    this.newMessage = {
      contactId: this.currentChat.contact.uid,
      createdAt: currentDate.getTime(),
      message: form.value.message,
      myUserId: this.user.uid,
      seen: false,
    };

    this.sendMessage(this.newMessage);
  }

  prepareChat(): void {
    if (this.currentChat.chat == null) {
      this.currentChat.chat = {
        contactId: '',
          hour_last_message: '',
          last_message: '',
          messages: null,
          messagesUrl: '',
          myUserId: '',
      }
    }
  }

}

/**
 * Created by jaimevillar on 02/23/18.
 */
import {NgModule} from '@angular/core';
import {ThemeModule} from '../../@theme/theme.module';
import {MessagesComponent} from './messages.component';
import {MessagesRoutingModule, routedComponents} from './messages-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    MessagesRoutingModule,
  ],
  declarations: [...routedComponents, MessagesComponent],
})
export class MessagesModule {
}

import {Component, forwardRef, HostBinding, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'ngx-munily-status-selector',
  templateUrl: './status-selector.component.html',
  styleUrls: ['./status-selector.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StatusSelectorComponent),
      multi: true,
    }],
})
export class StatusSelectorComponent<T> implements ControlValueAccessor {

  static nextId = 0;
  private _selected: T = null;

  @HostBinding() id = `app-image-${StatusSelectorComponent.nextId++}`;

  @Input() label: string;
  @Input() key: string = null;
  @Input() options: Array<T>;

  // @Output() onFileSelect: EventEmitter<File[]> = new EventEmitter();

  @Input() get value(): T | null {
    return this._selected || null;
  }

  set value(value: T | null) {
    this.writeValue(value);
  }

  get newLabel(): T | null {
    return this._selected ? (this.key ? this._selected[this.key] : this._selected) : this.label;
  }

  private _onChange(_: any) {
  }

  _onTouched() {
  }

  writeValue(obj: any): void {
    this._selected = <T>obj;
    this._onChange(obj);
  }

  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  onOptionSelected(option: T) {
    this.value = option;
    this._selected = option;
    this._onChange(this.value);
  }

}


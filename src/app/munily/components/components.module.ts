import {NgModule} from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { environment } from '../../../environments/environment';
import {ThemeModule} from '../../@theme/theme.module';

import {ImageSelectorComponent} from './image-selector/image-selector.component';
import {StatusSelectorComponent} from './status-selector/status-selector.component';
import { SearchMapComponent } from './search-map/search-map.component';
import { LocationService } from '../../@core/services';

const COMPONENTS = [
  ImageSelectorComponent,
  StatusSelectorComponent,
  SearchMapComponent,
];

@NgModule({
  imports: [
    ThemeModule,
    AgmCoreModule.forRoot({
      apiKey: environment.gmaps.apiKey,
      libraries: ['places'],
    }),
  ],
  declarations: [
    ...COMPONENTS,
  ],
  providers: [
    LocationService,
  ],
  exports: [...COMPONENTS],
})
export class ComponentsModule {
}

import {Component, ElementRef, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Location, Coordinates } from '../../../@core/models';
import { LocationService } from '../../../@core/services';

@Component({
  selector: 'ngx-munily-search-map',
  templateUrl: './search-map.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SearchMapComponent),
    multi: true,
  }],
})
export class SearchMapComponent implements OnInit, ControlValueAccessor {

  public location: Location = {
    address: null,
    point: {
      latitude: null,
      longitude: null,
    },
  };
  zoom = 14;

  @ViewChild('search') private searchInputRef: ElementRef;

  @Input()
  public get value(): Location | null {
    return this.location || null;
  }

  public set value(value: Location | null) {
    this.writeValue(value);
  }

  constructor(
    private locationService: LocationService) { }

  ngOnInit() {
    this.locationService.listenAutocomplete(this.searchInputRef).subscribe( location => {
      this.location = location;
      // console.log(`Location updates recevied ${location}`);
    });
  }

  mapClick(event) {

    const coords: Coordinates = {
      latitude: event.coords.lat,
      longitude: event.coords.lng,
    };

    this.writeValue({
      address: this.location.address,
      point: coords,
    });

    this.locationService.getAddressWithCoordinates(coords).subscribe( location => {
      this.writeValue(location);
    });
  }

  // ControlValueAccessor
  // ---------->

  onChange = (location: Location) => {};
  onTouched = () => {};

  writeValue(obj: any): void {
    // console.log(`Writing value ${obj}`);
    this.location = obj;
    this.onChange(obj);
  }

  registerOnChange(fn: (location: Location) => void): void {
    fn = this.onChange;
  }

  registerOnTouched(fn: () => void): void {
    fn = this.onTouched;
  }
}

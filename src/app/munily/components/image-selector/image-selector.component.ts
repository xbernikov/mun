import {
  Component, ElementRef, EventEmitter, forwardRef, HostBinding, HostListener, Input, OnInit, Output, Renderer2,
  ViewChild,
} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'ngx-munily-image-selector',
  templateUrl: './image-selector.component.html',
  styleUrls: ['./image-selector.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageSelectorComponent),
      multi: true,
    }],
})
export class ImageSelectorComponent implements ControlValueAccessor, OnInit {

  static nextId = 0;
  focused = false;

  private _files: File[];

  @ViewChild('imageInput') nativeInputFile: ElementRef;
  @HostBinding() id = `app-image-${ImageSelectorComponent.nextId++}`;

  @HostBinding('class.file-input-disabled') get isDisabled() {
    return this.disabled;
  }

  @Input() accept: string;
  @Output() onFileSelect: EventEmitter<File[]> = new EventEmitter();
  @Input() valuePlaceholder: string;
  @Input() multiple: boolean;


  @Input() get value(): Array<File> | null {
    return this.empty ? [] : this._files || [];
  }

  set value(fileInput: Array<File> | null) {
    this.writeValue(fileInput);
  }

  onBlur() {
    this._onTouched();
  }

  @Input() get disabled() {
    return this.nativeInputFile.nativeElement.disabled;
  }

  set disabled(disabled: boolean) {
    this.nativeInputFile.nativeElement.disabled = disabled;
  }


  get fileCount(): number {
    return this._files && this._files.length || 0;
  }

  get empty() {
    return !this.nativeInputFile.nativeElement.value || this.nativeInputFile.nativeElement.value.length === 0;
  }

  selectFile() {
    this.nativeInputFile.nativeElement.focus();
    this.focused = true;
    this._onTouched();
    this.open();
  }

  constructor(private elementRef: ElementRef,
              private renderer: Renderer2) {
  }

  private _onChange(_: any) {
  }

  private _onTouched() {
  }

  writeValue(obj: any): void {
    this._files = <Array<File>>obj;
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', obj);
    this._onChange(obj);
  }

  registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this._onTouched = fn;
  }

  @HostListener('change', ['$event']) change(event) {
    const fileList = event.target.files;
    const fileArray = [];
    if (fileList) {
      for (let i = 0; i < fileList.length; i++) {
        fileArray.push(fileList[i]);
      }
    }
    this.value = fileArray;
    this._files = fileArray;
    this.onFileSelect.emit(this._files);
    this._onChange(this.value);
  }

  setDisabledState?(isDisabled: boolean): void {
    this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', isDisabled);
  }

  ngOnInit() {
  }

  open() {
    if (!this.disabled) {
      this.nativeInputFile.nativeElement.click();
    }
  }

}


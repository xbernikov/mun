import {NgModule} from '@angular/core';

import {ThemeModule} from '../@theme/theme.module';
import {MunilyRoutingModule} from './munily-routing.module';
import {MunilyComponent} from './munily.component';


const MUNILY_COMPONENTS = [
  MunilyComponent,
];

@NgModule({
  imports: [
    MunilyRoutingModule,
    ThemeModule,
  ],
  declarations: [
    ...MUNILY_COMPONENTS,
  ],
})
export class MunilyModule {
}

import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AuthGuard} from './auth.guard';

const GUARDS = [
  AuthGuard,

];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...GUARDS,
  ],
})
export class GuardsModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: GuardsModule,
      providers: [
        ...GUARDS,
      ],
    };
  }
}

/**
 * Created by luis_arboleda17 on 02/25/18.
 */

import { Coordinates } from './coordinate.model';

export interface Location {
  address: string;
  point: Coordinates;
}

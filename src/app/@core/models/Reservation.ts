
export interface Reservation {
  uid: string;
  apartment: string;
  area: string;
  dateIn: number;
  dateOut: number;
  status: string;
  status_code: number;
}

import * as _ from 'lodash';

export interface Picture {
  key: string
  url: string;
  name: string;
}

export class Notice {
  $key?: string;
  title: string;
  message: string;
  status: string = 'creado';
  picture?: Picture = null;
  file: File;
  files: FileList;
  progress: number;
  createdAt: Date = new Date();
  updatedAt: Date = new Date();


  constructor(file: File) {
    this.file = file;
  }

  static fromForm(data: Notice): Notice {
    let notice: Notice = new Notice(null);

    if (data.files && data.files.length === 1) {
      notice = new Notice(data.files[0])
    }
    return _.extend(notice, _.omit(data, ['files']));
  }

  static prepare(notice: Notice): Notice {
    return <Notice>_.omit(notice, ['files', 'file', 'progress', '$key']);
  }

  static clone(notice: Notice): Notice {
    return _.clone(notice)
  }


  static clean(notice: Notice): Notice {
    let _notice: Object = {};

    for (const key in notice) {
      if (notice.hasOwnProperty(key) && [''].includes(key)) {
        if (!_.isNil(notice[`${key}`])) {
          switch (notice[`${key}`].constructor.name) {
            case Object.name:
              const object = notice[`${key}`];
              if (_.has(object, 'id') || _.has(object, '_id')) {
                _notice = _.extend(_notice, {[key]: object.id || object._id});
              }
              break;
            default:
              if (key === 'image') {
                _notice = _.extend(_notice, {[key]: (<Array<File>>notice[`${key}`])[0]});
                break;
              }
              _notice = _.extend(_notice, {[key]: notice[`${key}`]});
              break;
          }
        }
      }
    }
    return <Notice>_notice;
  }
}

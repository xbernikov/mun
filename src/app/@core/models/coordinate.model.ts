/**
 * Created by luis_arboleda17 on 02/25/18.
 */

export interface Coordinates {
  latitude: number;
  longitude: number;
}

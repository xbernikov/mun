import {Picture} from './notice';
import * as _ from 'lodash';

export enum Role {
  user = 'user',
  security = 'security',
  admin = 'admin',
  su = 'su',
}

export enum UsersPath {
  user = '/users',
  security = '/security',
  admin = '/admin',
}

export interface Apartment {
  building: string;
  apartment: string;
}

export class User {
  uid: string;
  $key: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;

  password?: string;

  roles: { [role: string]: true };
  $roles?: Array<string>;
  status: number;

  picture?: Picture = null;
  file: File;
  files: FileList;
  progress: number;

  buildings?: { [building: string]: true };
  $buildings?: Array<string>;

  apartments?: { [building: string]: string };
  $apartments?: Array<Apartment>;

  fcmToken: { [token: string]: true };

  createdAt: Date = new Date();
  updatedAt: Date = new Date();


  constructor(file: File) {
    this.file = file;
  }

  static fromForm(data: User): User {
    let user: User = new User(null);

    if (data.files && data.files.length === 1) {
      user = new User(data.files[0])
    }
    return _.extend(user, _.omit(data, ['files']));
  }

  static prepare(user: User): User {
    return <User>_.omit(user, ['files', 'file', 'progress', '$roles', '$buildings', '$apartments', '$key', 'password']);
  }
}

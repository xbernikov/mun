/**
 * Created by luis_arboleda17 on 02/22/18.
 */

import { Location } from './location.model';

export interface Building {
  key: string;
  name: string;
  location: Location;
  details: string;
  imageUrl: string;
  admins: [string];
  isActive: boolean;
  createdAt: number;
}

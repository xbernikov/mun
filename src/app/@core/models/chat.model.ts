/**
 * Created by jaimevillar on 02/23/18.
 */
interface Contact {
  name: string;
  uid: string;
}

export interface CurrentChat {
  exists: boolean;
  contact: Contact;
  chat: any;
}

export interface Chat {
  contactId: string,
    hour_last_message: number,
    last_message: string,
    messagesUrl: string,
    myUserId: string,
}

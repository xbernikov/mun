import * as _ from 'lodash';

/*interface Schedule {
    openingTime: number;
    closingTime: number;
}*/

/*interface Picture {
    key: string;
    url: string;
    name: string;
}*/

export class Area {
    $key: string;
    name: string;
    isBookable: boolean;
    // schedule: Array<Schedule>;
    status: string;
    // picture: Picture = null;
    // file: File;
    // files: FileList;
    created_at: Date = new Date();
    updated_at: Date = new Date();

    /*constructor(file: File) {
        this.file = file;
    }*/
  static prepare(area: Area): Area {
    return <Area>_.omit(area, ['$key']); // 'files', 'file', 'progress',
  }

  static fromForm(data: Area): Area {
    const area: Area = new Area();

    /*if (data.files && data.files.length === 1) {
      area = new Area(data.files[0])
    }*/
    return _.extend(area, _.omit(data)); // ['files']
  }
}

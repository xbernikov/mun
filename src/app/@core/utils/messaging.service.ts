import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import 'rxjs/add/operator/take';
import {BehaviorSubject} from 'rxjs/BehaviorSubject'

import {firebase} from '../../@firebase';
import 'firebase/messaging';

@Injectable()
export class MessagingService {

  messaging = firebase.messaging();
  currentMessage = new BehaviorSubject(null);

  constructor(private angularFire: AngularFireDatabase,
              private afAuth: AngularFireAuth) {
  }


  updateToken(token) {
    this.afAuth.authState.take(1).subscribe(user => {
      if (!user) return;

      const data = {[user.uid]: token};
      this.angularFire.object('fcmTokens/').update(data)
    })
  }

  getPermission() {
    this.messaging.requestPermission()
      .then(() => this.messaging.getToken())
      .then(token => this.updateToken(token))
      .catch((err) => console.error('Unable to get permission to notify.', err));
  }

  receiveMessage() {
    this.messaging.onMessage((payload) => {
      console.info('Message received. ', payload);
      this.currentMessage.next(payload)
    });

  }
}

import {Injectable} from '@angular/core';

@Injectable()
export class FileService {

  getImage(file: File): Promise<MSBaseReader> {
    return new Promise((resolve, reject) => {
      const reader: FileReader = new FileReader();
      reader.readAsDataURL(file);

      reader.onloadend = (event: ProgressEvent) => {
        resolve(reader.result);
      }
    });
  }

  getImageBase64(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      const image = document.createElement('img');

      reader.onload = () => {
        image.src = reader.result;
        resolve(this.getBase64Image(image));
      };

      reader.readAsDataURL(file);
    })
  }

  getBase64Image(img): string {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;

    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);

    return canvas.toDataURL('image/png');
  }
}

import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsService } from './analytics.service';
import {CommonModule} from '@angular/common';
import {AnalyticsService} from '@munily/core/utils/analytics.service';
import {ArrayService} from '@munily/core/utils/array.service';
import {MessagingService} from '@munily/core/utils/messaging.service';
import {PasswordService} from '@munily/core/utils/password.service';
import {StringService} from '@munily/core/utils/string.service';
import {FileService} from '@munily/core/utils/file.service';

const SERVICES = [
  AnalyticsService,
  ArrayService,
  MessagingService,
  PasswordService,
  StringService,
  FileService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  providers: [
    ...PROVIDERS,
  ],
})
export class UtilsModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: UtilsModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}

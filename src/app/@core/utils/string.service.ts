import { Injectable } from '@angular/core';

@Injectable()
export class StringService {

  static slugify(text: string, separator: string = '-') {
    return text
      .toString()
      .toLowerCase()
      .replace(/\s+/g, separator)           // Replace spaces with -
      .replace(/[^\w-]+/g, '')       // Remove all non-word chars
      .replace(/--+/g, separator)         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')
      .trim();
  }

  static htmlToText(html: string) {
    return html
      .replace(/<(?:.|\n)*?>/gm, '')
      .replace(/&nbsp;/gm, ' ')
      .replace(/&quot;/gm, '"')
      .replace(/&amp;/gm, '&')
      .replace(/&gt;/gm, '>')
      .replace(/&lt;/gm, '<')
      .replace(/&iquest;/gm, '¿')
      .replace(/&Aacute;/gm, 'Á')
      .replace(/&Eacute;/gm, 'É')
      .replace(/&Iacute;/gm, 'Í')
      .replace(/&Oacute;/gm, 'Ó')
      .replace(/&Uacute;/gm, 'Ú')
      .replace(/&aacute;/gm, 'á')
      .replace(/&eacute;/gm, 'é')
      .replace(/&iacute;/gm, 'í')
      .replace(/&oacute;/gm, 'ó')
      .replace(/&uacute;/gm, 'ú')
      .replace(/&ntilde;/gm, 'ñ')
      .replace(/&ldquo;/gm, 'ñ')
      .replace(/&ldquo;/gm, '“')
      .replace(/&rdquo;/gm, '”')
      .replace(/&bdquo;/gm, '„')
      .replace(/&ndash;/gm, '–')
      .replace(/&mdash;/gm, '—')
      .replace(/&iexcl;/gm, '¡')
      .replace(/&rsquo;/gm, '’')
  }

  static slugifyDotsUnderlines(text: string, separator: string = '-') {
    return text.toString().toLowerCase()
      .replace(/\s+/g, separator)           // Replace spaces with -
      .replace(/[^\w_.]+/g, '')       // Remove all non-word chars
      .replace(/--+/g, separator)         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')
      .trim();            // Trim - from end of text
  }

  static accentsTidy(text: string) {
    return text.toString().toLowerCase()
      .replace(/[àáâãäå]/g, 'a')
      .replace(/[èéêë]/g, 'e')
      .replace(/[[ìíîï]/g, 'i')
      .replace(/[òóôõö]/g, 'o')
      .replace(/[ùúûü]/g, 'u')
      .trim();
  }

  static truncate(text: string, length: number = 50) {
    return `${text.substr(0, Math.min(length, text.length))}${Math.min(length, text.length) === length ? '...' : ''}`;
  }
}

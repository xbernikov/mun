import {Injectable} from '@angular/core';

@Injectable()
export class PasswordService {
  static pattern: RegExp = /[a-zA-Z0-9_]/;
  // static pattern: RegExp = /[a-zA-Z0-9_\-\+\.]/;


  static getRandomByte(): number {
    return Math.floor(Math.random() * 256);
  }

  static generate(length): string {
    return Array.apply(null, {'length': length}).map(() => {
      let result: string;
      while (true) {
        result = String.fromCharCode(PasswordService.getRandomByte());
        if (PasswordService.pattern.test(result)) return result;
      }
    }).join('');
  }
}

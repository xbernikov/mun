import {Injectable} from '@angular/core';

@Injectable()
export class ArrayService {
  static containsSome(superset: Array<any>, subset: Array<any> = []): boolean {
    if (0 === subset.length) {
      return false;
    }

    return subset.some((value) => superset.includes(value));
  }
}


import {ElementRef, Injectable, NgZone} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MapsAPILoader } from '@agm/core';
import {} from 'googlemaps';

import { Location, Coordinates } from '../models';


@Injectable()
export class LocationService {

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  /**
   * Listen to address changes in an input
   * @param elementRef
   * @returns {Observable<Location>|"../../Observable".Observable<Location>|"../../../Observable".Observable<Location>}
   */
  listenAutocomplete(elementRef: ElementRef): Observable<Location> {
    return new Observable<Location>( observer => {
      this.mapsAPILoader.load().then( () => {
        const autocomplete: google.maps.places.Autocomplete = new google.maps.places.Autocomplete(
          elementRef.nativeElement, {
            componentRestrictions: {
              country: 'pa',
            },
          });

        autocomplete.addListener('place_changed', () => {
          this.ngZone.run(() => {
            const place: google.maps.places.PlaceResult = autocomplete.getPlace();

            if (place.geometry === undefined || place.geometry === null) {
              observer.error('Place is undefined');
              return;
            }

            const placeLocation = place.geometry.location;
            const coordinates: Location = {
              address: place.formatted_address,
              point: {
                latitude: placeLocation.lat(),
                longitude: placeLocation.lng(),
              },
            };

            observer.next(coordinates);
            observer.complete();
          });
        });
      });
    });
  }

  /**
   * Get address in specified coordinate
   * @param coordinates
   * @returns {Observable<Location>|"../../Observable".Observable<Location>|"../../../Observable".Observable<Location>}
   */
  getAddressWithCoordinates(coordinates: Coordinates): Observable<Location> {
    return new Observable<Location>( observer => {
      this.mapsAPILoader.load().then( () => {

        const geocoder: google.maps.Geocoder = new google.maps.Geocoder();

        geocoder.geocode({
          location: {
            lat: coordinates.latitude,
            lng: coordinates.longitude,
          },
        }, (results, status) => {
          if (status === google.maps.GeocoderStatus.OK && results[1]) {
            const address = results[1].formatted_address;
            observer.next({
              address: address,
              point: coordinates,
            });
            observer.complete();
          } else {
            observer.error(`Geocoder received status: ${status}`);
          }
        });
      });
    });
  }
}

import {Injectable} from '@angular/core';
import 'rxjs/add/observable/fromPromise';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, AngularFireList, SnapshotAction} from 'angularfire2/database';
import {Notice} from '../models';

import {firebase} from '../../@firebase';
import 'firebase/storage';
import {StringService} from '../utils';


@Injectable()
export class NoticeService {

  basePath = 'notices';
  noticesRef: AngularFireList<Notice>;

  constructor(private angularFire: AngularFireDatabase) {
  }

  getByBuilding(building: string): Observable<Notice[]> {
    return this.angularFire.list(`/${this.basePath}/${building}`)
      .snapshotChanges()
      .map((actions: Array<SnapshotAction>) => {
        return actions.map((a) => {
          const data = a.payload.val();
          const $key = a.payload.key;
          return {$key, ...data};
        });
      });
  }

  get(key: string): Observable<Notice> {
    if (!key) return Observable.of(new Notice(null));

    return this.angularFire.object(`/${this.basePath}/${key}`)
      .snapshotChanges()
      .map((action: SnapshotAction) => {
        const data = action.payload.val();
        const $key = action.payload.key;
        return {$key, ...data};
      });

  }

  getAll(): Observable<Notice[]> {
    return this.angularFire.list(`/${this.basePath}`, ref => ref.orderByChild('updatedAt'))
      .snapshotChanges().map((actions) => {
        return actions.map((a) => {
          const data = a.payload.val();
          const $key = a.payload.key;
          return {$key, ...data};
        });
      });
  }


  save(notice: Notice, update: boolean = false): Observable<any> {
    return update ? this.update(notice) : Observable.fromPromise(this.angularFire.list(`/${this.basePath}/`)
      .push(Notice.prepare(notice)));
  }

  push(notice: Notice, update: boolean = false): Observable<void> {
    return Observable.fromPromise(new Promise((resolver, rejecter) => {

      if (!notice.file) return this.save(notice, update).subscribe(() => resolver(), err => rejecter(err));

      this.uploadPicture(notice)
        .subscribe((data) => {
          this.save(data, update).subscribe(() => resolver(), err => rejecter(err));
        }, err => rejecter(err));
    }));
  }

  private uploadPicture(notice: Notice): Observable<Notice> {
    return Observable.fromPromise(new Promise((resolver, rejecter) => {

      this.deleteImageFromStorage(notice)
        .subscribe(
          () => console.info('image deleted'),
          err => console.error(err),
        );
      const storageRef = firebase.storage().ref();
      const key = StringService.slugify(notice.file.name);

      const uploadTask = storageRef.child(`${this.basePath}/${key}`).put(notice.file);

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: firebase.storage.UploadTaskSnapshot) =>
          notice.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100,
        (error) => rejecter(error),
        () => {

          if (!uploadTask.snapshot.downloadURL) return rejecter(new Error('No download URL!'));

          notice.picture = {
            key,
            url: uploadTask.snapshot.downloadURL,
            name: notice.file.name,
          };

          resolver(notice);
        },
      );
    }));
  }

  replace(notice: Notice) {
    const noticeRef: AngularFireList<Notice> = this.angularFire.list(`/${this.basePath}`);
    return Observable.fromPromise(noticeRef.set(notice.$key, notice));
  }

  update(notice: Notice) {
    const data = Notice.clone(notice);
    const noticeRef: AngularFireList<Notice> = this.angularFire.list(`/${this.basePath}`);
    return Observable.fromPromise(noticeRef.update(notice.$key, Notice.prepare(data)));
  }

  remove(notice: Notice) {
    this.removeData(notice)
      .subscribe(
        () => this.deleteImageFromStorage(notice),
        err => console.error(err),
      )
  }

  removeData(notice: Notice): Observable<void> {
    return Observable.fromPromise(this.angularFire.list(`/${this.basePath}/`).remove(notice.$key));
  }

  private deleteImageFromStorage(notice: Notice): Observable<void> {
    if (notice.picture) {
      const storeRef: firebase.storage.Reference = firebase.storage().ref();
      return Observable.fromPromise(storeRef.child(`${this.basePath}/${notice.picture.key}`).delete());
    }
    return Observable.of(null);
  }
}

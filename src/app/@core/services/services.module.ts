import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { AgmCoreModule } from '@agm/core';

import { environment } from '../../../environments/environment';

import {AuthService} from './auth.service';
import {BuildingService} from './building.service';
import {UploadService} from './upload.service';
import { LocationService } from './location.service';
import {ReservationService} from './reservation.service';
import {NoticeService} from './notice.service';
import {UserService} from './user.service';
import {MessageService} from './message.service';

const SERVICES = [
  AuthService,
  BuildingService,
  UploadService,
  LocationService,
  ReservationService,
  NoticeService,
  UserService,
  MessageService,
];

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: environment.gmaps.apiKey,
      libraries: ['places'],
    }),
  ],
  providers: [
    ...SERVICES,
  ],
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: ServicesModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}

import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, SnapshotAction } from 'angularfire2/database';
import { Area } from '../models/';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'firebase/storage';

@Injectable()
export class AreaService {

  basePath = 'areas';
  areas: AngularFireList<Area>;

  constructor(private db: AngularFireDatabase) {
  }

  getAll() {
    return this.db.list(`/${this.basePath}`, ref => ref.orderByChild('updatedAt'))
      .snapshotChanges().map((actions) => {
        console.info(actions);
        return actions.map((a) => {
          const data = a.payload.val();
          const $key = a.payload.key;
          return {$key, ...data};
        });
      });
  }

  get(key: string): Observable<Area> {
    if (!key) return Observable.of(new Area());

    return this.db.object(`/${this.basePath}/${key}`)
      .snapshotChanges()
      .map((action: SnapshotAction) => {
        const data = action.payload.val();
        const $key = action.payload.key;
        return {$key, ...data};
      });
  }

  create(area: Area) {
    return Observable.fromPromise(this.db.list(`/${this.basePath}/`).push(Area.prepare(area)));
  }

  update(area: Area) {
    const areaRef: AngularFireList<Area> = this.db.list(`/${this.basePath}`);
    return Observable.fromPromise(areaRef.update(area.$key, Area.prepare(area)));
  }

  save(area: Area, update: boolean = false): Observable<any> {
    return update ? this.update(area) : Observable.fromPromise(this.db.list(`/${this.basePath}/`)
      .push(Area.prepare(area)));
  }

  remove(area: Area): Observable<void> {
    return Observable.fromPromise(this.db.list(`/${this.basePath}/${area.$key}`).remove());
  }

}

import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Reservation} from '../models/Reservation';


@Injectable()
export class ReservationService {

  basePath = '/areas_reserved';
  private reservations: AngularFireList<Reservation>;

  constructor(private database: AngularFireDatabase) {
    this.reservations = this.database.list<Reservation>(this.basePath);
  }

  getAreasReserved(): Observable<Reservation[]> {
    return this.reservations.snapshotChanges().map((actions) => {
      return actions.map((action) => {
        const data = action.payload.val();
        const uid = action.payload.key;
        return {uid, ...data};
      });
    })
  }

  update(reservation: Reservation) {
    this.reservations.update(reservation.uid, reservation)
  }

}

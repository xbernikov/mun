/**
 * Created by jaimevillar on 02/23/18.
 */

import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from './auth.service';
import {Observable} from 'rxjs/Observable';
import { Chat } from '../models/chat.model';

@Injectable()
export class MessageService {

  private chatPath = '/chat';
  private usersPath = '/users';
  auth: AuthService;

  constructor(private database: AngularFireDatabase) {
  }

  getContactListByBuilding(building: string): Observable<any> {
    return this.database.list(`${this.usersPath}`, ref => ref.orderByChild('build').equalTo(building)).valueChanges();
    // return this.database.list(`${this.usersPath}/buildings`,
    //  ref => ref.orderByChild(building).equalTo(true)).valueChanges();
  }

  getChat(userId: string, contactId: string): Observable<Chat> {
    return this.database.object<Chat>(`${this.chatPath}/me-${ userId }/with-${ contactId }`).valueChanges();
  }

  getMessages(messagesUrl: string): Observable<any> {
    // return this.database.list(`${ messagesUrl }/messages`, ref => ref.limitToLast(2)).valueChanges();
    return this.database.list(`${ messagesUrl }/messages`).valueChanges();
  }

  createChat(userId: string, contactId: string, messageObj: any) {
    this.database.list(`${this.chatPath}/me-${ userId }`)
      .set('with-' + contactId, {
        contactId: contactId,
        hour_last_message: messageObj.createdAt,
        last_message: messageObj.message,
        messagesUrl: `${this.chatPath}/me-${ userId }/with-${ contactId }`,
        myUserId: userId,
      })
      .then(_ => console.info('created user chat'))
      .catch(err => console.error(err, 'error creating user chat'));

    this.database.list(`${this.chatPath}/me-${ userId }/with-${ contactId }/messages`)
      .push(messageObj);

    this.database.list(`${this.chatPath}/me-${ contactId }`)
      .set('with-' + userId, {
        contactId: userId,
        hour_last_message: messageObj.createdAt,
        last_message: messageObj.message,
        messagesUrl: `${this.chatPath}/me-${ userId }/with-${ contactId }`,
        myUserId: contactId,
      })
      .then(_ => console.info('created contact chat'))
      .catch(err => console.error(err, 'error creating contact chat'));
  }

  updateChat(userId: string, contactId: string, messagesUrl: string, messageObj: any) {
    this.database.list(`${this.chatPath}/me-${ userId }`)
      .update('/with-' + contactId, {
        hour_last_message: messageObj.createdAt,
        last_message: messageObj.message,
      })
      .then(_ => console.info('updated user chat'))
      .catch(err => console.error(err, 'error updating user chat'));

    this.database.list(`${ messagesUrl }/messages`)
      .push(messageObj);

    this.database.list(`${this.chatPath}/me-${ contactId }`)
      .update('/with-' + userId, {
        hour_last_message: messageObj.createdAt,
        last_message: messageObj.message,
      })
      .then(_ => console.info('updated contact chat'))
      .catch(err => console.error(err, 'error updating contact chat'));
  }

}

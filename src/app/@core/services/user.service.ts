import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject, SnapshotAction} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {User} from '../models';

@Injectable()
export class UserService {

  basePath: string = 'users/'
  users: AngularFireList<User> = null;

  constructor(private database: AngularFireDatabase) {
  }

  getUsers(path = this.basePath): Observable<User[]> {
    return this.database.list<User>(path).valueChanges();
  }

  get(basePath: string, key: string): Observable<User> {
    if (!key) return Observable.of(new User(null));

    return this.database.object(`${basePath}${key}`)
      .snapshotChanges()
      .map((action: SnapshotAction) => {
        const data = action.payload.val();
        const $key = action.payload.key;
        return {$key, ...data};
      });
  }

  create(user: User): void {
    this.users.push(user)
  }

  update(key: string, value: any): void {
    this.users.update(key, value)
  }

  remove(key: string): void {
    this.users.remove(key)
  }

  updateUserData(user: any, data: User): Observable<any> {
    const userRef: AngularFireObject<User> = this.database.object( `${this.basePath}${user.uid}`);
    return Observable.fromPromise(userRef.set(
      _.extend(data, _.pick(user, ['uid', 'emial', 'displayName', 'photoURL'])),
    ));
  }
}

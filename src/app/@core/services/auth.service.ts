import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import 'rxjs/add/observable/fromPromise';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, SnapshotAction} from 'angularfire2/database';
import * as _ from 'lodash';

import {firebase} from '../../@firebase';
import {User} from '../models';

@Injectable()
export class AuthService {
  public user: Observable<User | null>;

  constructor(private firebaseAuth: AngularFireAuth,
              private angularFire: AngularFireDatabase) {

    this.user = this.firebaseAuth.authState
      .switchMap((user) => user ? this.angularFire.object(`users/${user.uid}`).snapshotChanges()
        .map((action: SnapshotAction) => {
          const data = action.payload.val();
          const $key = action.payload.key;
          let rtn = {$key, ...data};
          if (!_.isEmpty(data.buildings)) {
            rtn = _.extend(rtn, {
              $buildings: Object.keys(data.buildings),
            });
          }
          if (!_.isEmpty(data.roles)) {
            rtn = _.extend(rtn, {
              $roles: Object.keys(data.roles),
            });
          }
          if (!_.isEmpty(data.apartments)) {
            rtn = _.extend(rtn, {
              $apartments: _.map(data.apartments, (v, k) => ({building: k, apartment: v})),
            });
          }
          return rtn;
        }) : Observable.of(null));
  }

  signInRegular({email, password}): Observable<User> {
    return Observable.fromPromise(this.firebaseAuth.auth.signInWithEmailAndPassword(email, password));
  }

  resetPassword(email: string) {
    const fbAuth = firebase.auth();
    return fbAuth.sendPasswordResetEmail(email);
  }

  signOut(): Observable<User> {
    return Observable.fromPromise(this.firebaseAuth.auth.signOut());
  }
}

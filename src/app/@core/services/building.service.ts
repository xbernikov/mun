/**
 * Created by luis_arboleda17 on 02/22/18.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { StringService } from '../utils/string.service';
import { Building, Coordinates } from '../models';
import {AuthService} from './auth.service';

@Injectable()
export class BuildingService {

  private PATH = '/buildings';
  private BASE_KEY = 'building-';
  // private USERS_PATH = 'users/';

  private buildings: AngularFireList<Building>;

  constructor(
    private database: AngularFireDatabase,
    private authService: AuthService) {
    this.buildings = this.database.list(this.PATH);
  }

  // CRUD Operations

  /**
   * Get building with key
   * @param key
   * @returns {Observable<null|Building>}
   */
  getBuilding(key: string): Observable<Building> {
    return this.database.object<Building>(`${this.PATH}/${key}`).valueChanges();
  }

  /**
   * Get buildings. Can specify the status or leave it blank
   * @param active
   * @returns {Observable<T[]>}
   */
  getBuildings(statusActive?: boolean): Observable<Building[]> {

    let query;

    if (statusActive) {
      query = ref => {
        ref.orderByChild('active').equalTo(statusActive);
      };
    }

    return this.database.list<Building>(this.PATH, query).valueChanges();
  }

  /**
   * Create a building and add to database
   * @param name
   * @param details
   * @param address
   * @param coordinates
   * @param active
   * @returns {Observable|"../../Observable".Observable|"../../../Observable".Observable}
   */
  create(name: string, details: string, address: string, coordinates: Coordinates, active: boolean): Observable<any> {
    return new Observable( observer => {
      this.authService.user.subscribe( user => {

        const building: Building = {
          key: StringService.slugify(`${this.BASE_KEY}${name}`),
          name: name,
          location: {
            address: address,
            point: coordinates,
          },
          details: details,
          imageUrl: null,
          admins: [user.uid],
          isActive: active,
          createdAt: Date.now(),
        };

        Observable.fromPromise(this.buildings.set(building.key, building)).subscribe( response => {
          observer.next(response);
          observer.complete();
        }, error => {
          throw Observable.throw(error);
        });
      }, error => {
        throw Observable.throw(error);
      });
    });
  }

  /**
   * Edit a building with specific key
   * @param building
   * @returns {any}
   */
  edit(building: Building): Observable<any> {
    return Observable.fromPromise(this.buildings.set(building.key, building));
  }

  /**
   * Delete a building with specific key
   * @param id
   * @returns {any}
   */
  remove(key: string): Observable<any> {
    return Observable.fromPromise(this.buildings.remove(key));
  }

  // Additional operations

  /**
   * Add an admin to a building
   * @param buildingKey
   * @param adminKey
   * @returns {Observable|"../../../Observable".Observable|"../../Observable".Observable}
   */
  addAdmin(buildingKey: string, adminKey: string): Observable<any> {
    const self = this;
    return new Observable(observer => {
      return self.database.object(`${this.PATH}/${buildingKey}`).valueChanges().subscribe( (building: Building) => {
        building.admins.push(adminKey);
        return self.edit(building);
      });
    });
  }

  /**
   * Get number of active buildings
   * @returns {Observable<number>|"../../../Observable".Observable<number>|"../../Observable".Observable<number>}
   */
  getNumberOfActiveBuildings(): Observable<number> {
    return new Observable<number>(observer => {
      this.getBuildings(true).subscribe(buildings => {
        observer.next(buildings.map( (building) => {
          if (building.isActive) {
            return building
          }
        }).length);
        observer.complete();
      });
    });
  }
}

import {Component} from '@angular/core';

import {MENU_ITEMS} from './pages-menu';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-base-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-base-layout>
  `,
  styleUrls: ['./pages.component.scss'],
})
export class PagesComponent {

  menu = MENU_ITEMS;
}

import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import { Coordinates } from '../../../../@core/models';

@Component({
  selector: 'ngx-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  latitude: number;
  longitude: number;
  zoom: number;

  @Input()
  public set searchedLocation(searchedLocation: Coordinates) {
    this.latitude = searchedLocation.latitude;
    this.longitude = searchedLocation.longitude;
    this.zoom = 14;
  }

  @Output() positionChanged = new EventEmitter<Coordinates>();

  ngOnInit(): void {
    // set up current location
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.searchedLocation = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        };
      });
    }
  }

  mapEventHandler(event) {
    const coordinates = event.coords;
    this.positionChanged.emit({ latitude: coordinates.lat, longitude: coordinates.lng });
  }
}

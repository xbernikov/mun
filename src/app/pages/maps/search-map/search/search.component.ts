import {Component, ElementRef, EventEmitter, NgZone, OnInit, Output, ViewChild} from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Coordinates } from '../../../../@core/models';

@Component({
  selector: 'ngx-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  @ViewChild('search')
  public searchElementRef: ElementRef;

  @Output() locationChanged = new EventEmitter<Coordinates>();

  constructor(private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) {
  }

  ngOnInit() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address'],
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          const coordinates = place.geometry.location;
          this.locationChanged.emit({ latitude: coordinates.lat(), longitude: coordinates.lng() });
        });
      });
    });
  }
}

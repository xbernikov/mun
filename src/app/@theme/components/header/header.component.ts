import {Component, Input} from '@angular/core';
import {NbMenuService, NbSidebarService} from '@nebular/theme';

import {AuthService} from '../../../@core/services';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent {


  @Input() position = 'normal';

  user: any;

  userMenu = [{title: 'Profile'}, {title: 'Edificios'}, {title: 'Log out', link: '/auth/logout'}];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              public authService: AuthService) {
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }


}

import {Component} from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created with ♥ by <b>
      <a href="http://nueraly.com" target="_blank">Nueraly</a>
    </b> using <b>
      <a href="https://akveo.github.io/nebular/" target="_blank">Nebular</a>
    </b> 2018
    </span>
    <div class="socials">
      <a href="https://www.instagram.com/munilyapp/" target="_blank" class="fa fa-instagram"></a>
      <a href="https://www.facebook.com/munilyapp/" target="_blank" class="fa fa-facebook"></a>
      <a href="https://plus.google.com/105562664272667105378" target="_blank" class="fa fa-google-plus-square"></a>
    </div>
  `,
})
export class FooterComponent {
}

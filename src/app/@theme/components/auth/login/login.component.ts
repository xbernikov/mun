/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {getDeepFromObject} from '@nebular/auth/helpers';
import {AuthService} from '../../../../@core/services';
import {NB_AUTH_OPTIONS_TOKEN} from '@nebular/auth';


@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent implements OnInit {

  redirectDelay: number = 0;
  showMessages: any = {};

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;

  constructor(private authService: AuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {
  }

  ngOnInit(): void {
    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
  }

  login(): void {
    this.errors = this.messages = [];
    this.submitted = true;

    this.authService.signInRegular(this.user)
      .subscribe((user) => {
        this.submitted = false;
        setTimeout(() => {
          return this.router.navigateByUrl('/munily');
        }, this.redirectDelay);
      }, (error) => {
        this.submitted = false;
        this.errors = [error.message];
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

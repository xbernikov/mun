/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {Component, Inject, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../../@core/services';
import {NB_AUTH_OPTIONS_TOKEN} from '@nebular/auth';
import {getDeepFromObject} from '@nebular/auth/helpers';
import {User} from '../../../../@core/models';


@Component({
  selector: 'ngx-munily-logout',
  template: `
    <div>Logging out, please wait...</div>`,
})
export class NgxLogoutComponent implements OnInit {

  redirectDelay: number = 0;
  provider: string = '';

  constructor(protected authService: AuthService,
              @Inject(NB_AUTH_OPTIONS_TOKEN) protected config = {},
              protected router: Router) {
    this.redirectDelay = this.getConfigValue('forms.logout.redirectDelay');
    this.provider = this.getConfigValue('forms.logout.provider');
  }

  ngOnInit(): void {
    this.logout();
  }

  logout(): void {
    this.authService.signOut().subscribe((user: User) => {
      setTimeout(() => {
        return this.router.navigate(['/auth/login']);
      }, this.redirectDelay);
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}

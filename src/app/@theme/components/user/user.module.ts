/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import {NgModule} from '@angular/core';

import {NbBadgeModule} from '@nebular/theme';
import {NbSharedModule} from '@nebular/theme/components/shared/shared.module';
import {NgxUserComponent} from './user.component';


const NB_USER_COMPONENTS = [
  NgxUserComponent,
];

@NgModule({
  imports: [
    NbSharedModule,
    NbBadgeModule,
  ],
  declarations: [
    ...NB_USER_COMPONENTS,
  ],
  exports: [
    ...NB_USER_COMPONENTS,
  ],
})
export class NgxUserModule {
}
